package com.devcamp.pizza_api.models;

import java.util.Date;

public class Drink {
    private String maNuocUong;
    private String tenNuocUong;
    private int donGia;
    private String ghiChu = null;
    private Date ngayTao;
    private Date ngayCapNhat;

    public Drink(String maNuocUong, String tenNuocUong, int donGia, String ghiChu, Date ngayTao, Date ngayCapNhat) {
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ghiChu = ghiChu;
        this.ngayTao = new Date();
        this.ngayCapNhat = new Date();
    }

    public String getMaNuocUong() {
        return maNuocUong;
    }

    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }

    public String getTenNuocUong() {
        return tenNuocUong;
    }

    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    @Override
    public String toString() {
        return "Drink [maNuocUong=" + maNuocUong + ", tenNuocUong=" + tenNuocUong + ", donGia=" + donGia + ", ghiChu="
                + ghiChu + ", ngayTao=" + ngayTao + ", ngayCapNhat=" + ngayCapNhat + "]";
    }
    

}
