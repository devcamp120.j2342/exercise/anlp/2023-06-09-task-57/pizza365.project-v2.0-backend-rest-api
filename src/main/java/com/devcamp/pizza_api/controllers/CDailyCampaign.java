package com.devcamp.pizza_api.controllers;

import java.time.LocalDate;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CDailyCampaign {
        @GetMapping("/campaigns")
    public String getDailyCampaign() {
        String dayOfWeek = LocalDate.now().getDayOfWeek().name();
        String campaign = "";
        
        switch (dayOfWeek) {
            case "MONDAY":
                campaign = "Mua 1 combo lớn tặng 1 pizza cỡ nhỏ";
                break;
            case "TUESDAY":
                campaign = "Tặng tất cả khách hàng một phần bánh ngọt";
                break;
            // Thêm các trường hợp khác tương ứng với các ngày trong tuần
            default:
                campaign = "Không có khuyến mại";
                break;
        }
        
        return campaign;
    }
}
