package com.devcamp.pizza_api.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza_api.models.Menu;
import com.devcamp.pizza_api.services.MenuService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CComboMenu {
    @Autowired
    MenuService menuService;

    @CrossOrigin
    @GetMapping("/combomenu")
    public ArrayList<Menu> getAllMenus() {
        return  menuService.getAllMenusService();
    }    

}
