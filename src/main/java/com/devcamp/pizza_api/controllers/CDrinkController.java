package com.devcamp.pizza_api.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza_api.models.Drink;
import com.devcamp.pizza_api.services.DrinkService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CDrinkController {
    @Autowired
    DrinkService drinkService;

    @GetMapping("/drinks")

    public ArrayList<Drink> getAllDrink() {
        return drinkService.getArrayListDrink();
    }

}
