package com.devcamp.pizza_api.services;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.devcamp.pizza_api.models.Drink;

@Service
public class DrinkService {
    Drink drink1 = new Drink("TRATAC", "Tra Tac", 10000, null, new Date(), new Date());
    Drink drink2 = new Drink("COCA", "CoCaCoLa", 15000, null, new Date(), new Date());
    Drink drink3 = new Drink("PESSI", "Pessi", 15000, null, new Date(), new Date());
    Drink drink4 = new Drink("LAVIE", "Lavie", 5000, null, new Date(), new Date());
    Drink drink5 = new Drink("TRASUA", "Tra Sua", 40000, null, new Date(), new Date());
    Drink drink6 = new Drink("FANTA", "Fanta", 15000, null, new Date(), new Date());

    public ArrayList<Drink> getArrayListDrink() {
        ArrayList<Drink> allDrink = new ArrayList<>();

        allDrink.add(drink1);
        allDrink.add(drink2);
        allDrink.add(drink3);
        allDrink.add(drink4);
        allDrink.add(drink5);
        allDrink.add(drink6);

        return allDrink;
    }

}
