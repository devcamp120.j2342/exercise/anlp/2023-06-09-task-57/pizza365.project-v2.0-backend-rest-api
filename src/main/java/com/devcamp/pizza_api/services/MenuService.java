package com.devcamp.pizza_api.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.pizza_api.models.Menu;

@Service
public class MenuService {
    
    Menu sizeS = new Menu('S', 20, 2, 200, 2, 150000);
    Menu sizeM = new Menu('M', 25, 4, 300, 3, 200000);
    Menu sizeL = new Menu('L', 30, 8, 500, 4, 250000);

    public ArrayList<Menu> getAllMenusService() {
        ArrayList<Menu> allMenus = new ArrayList<>();

        allMenus.add(sizeS);
        allMenus.add(sizeM);
        allMenus.add(sizeL);

        return allMenus;
    }

}
